-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Feb 2021 pada 22.12
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `book`
--

CREATE TABLE `book` (
  `id` varchar(255) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `release_date` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `book`
--

INSERT INTO `book` (`id`, `age`, `author`, `price`, `publisher`, `release_date`, `title`) VALUES
('4028ab3e77a1962c0177a1977a5a0000', 7, 'Fujiko F. Fujio', 59000, 'Gramedia', '2020-02-02 00:00:00', 'Doraemon'),
('4028ab3e77a1962c0177a1977a750001', 13, NULL, 64000, 'Gramedia', '2012-12-12 00:00:00', 'Naruto'),
('4028ab3e77a1bd4f0177a1bffa860000', 4, NULL, 53000, 'Gramedia', '2011-11-11 00:00:00', 'Dora The Explorer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `book_genres`
--

CREATE TABLE `book_genres` (
  `books_id` varchar(255) NOT NULL,
  `genres_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `book_genres`
--

INSERT INTO `book_genres` (`books_id`, `genres_id`) VALUES
('4028ab3e77a1962c0177a1977a5a0000', '4028ab3e77a1d9320177a1dd18020000'),
('4028ab3e77a1962c0177a1977a5a0000', '4028ab3e77a1d9320177a1dd185f0002'),
('4028ab3e77a1962c0177a1977a5a0000', '4028ab3e77a1d9320177a1dd18600004'),
('4028ab3e77a1962c0177a1977a750001', '4028ab3e77a1d9320177a1dd185f0002'),
('4028ab3e77a1962c0177a1977a750001', '4028ab3e77a1d9320177a1dd185f0003'),
('4028ab3e77a1962c0177a1977a750001', '4028ab3e77a1d9320177a1dd18600004'),
('4028ab3e77a1bd4f0177a1bffa860000', '4028ab3e77a1d9320177a1dd185f0002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `book_users`
--

CREATE TABLE `book_users` (
  `books_id` varchar(255) NOT NULL,
  `users_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `genre`
--

CREATE TABLE `genre` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `genre`
--

INSERT INTO `genre` (`id`, `name`) VALUES
('4028ab3e77a1d9320177a1dd18020000', 'Fantasy'),
('4028ab3e77a1d9320177a1dd185e0001', 'Slice Of life'),
('4028ab3e77a1d9320177a1dd185f0002', 'Adventure'),
('4028ab3e77a1d9320177a1dd185f0003', 'Action'),
('4028ab3e77a1d9320177a1dd18600004', 'Comedy'),
('4028ab3e77a2229d0177a259d8080000', 'Sci-Fi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `balance` int(11) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `balance`, `birth_date`, `name`, `password`, `username`) VALUES
('4028ab3e77a2229d0177a25df1ea0001', 500000, '1997-08-09', 'Yosef', NULL, 'yosef');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `book_genres`
--
ALTER TABLE `book_genres`
  ADD KEY `FKkemwddl6cxkebe21lsqa2ob4q` (`genres_id`),
  ADD KEY `FKlbdkit5k0gr9g1w5l791qcamg` (`books_id`);

--
-- Indeks untuk tabel `book_users`
--
ALTER TABLE `book_users`
  ADD KEY `FKg6rj25t569st6f3jf7ty33otl` (`users_id`),
  ADD KEY `FKalyar5vip5sr9dawnoqtn7a17` (`books_id`);

--
-- Indeks untuk tabel `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `book_genres`
--
ALTER TABLE `book_genres`
  ADD CONSTRAINT `FKkemwddl6cxkebe21lsqa2ob4q` FOREIGN KEY (`genres_id`) REFERENCES `genre` (`id`),
  ADD CONSTRAINT `FKlbdkit5k0gr9g1w5l791qcamg` FOREIGN KEY (`books_id`) REFERENCES `book` (`id`);

--
-- Ketidakleluasaan untuk tabel `book_users`
--
ALTER TABLE `book_users`
  ADD CONSTRAINT `FKalyar5vip5sr9dawnoqtn7a17` FOREIGN KEY (`books_id`) REFERENCES `book` (`id`),
  ADD CONSTRAINT `FKg6rj25t569st6f3jf7ty33otl` FOREIGN KEY (`users_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
