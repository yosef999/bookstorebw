package com.yosefbw.demo.controller;

import com.yosefbw.demo.entity.User;
import com.yosefbw.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/getAll")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/getAllByName/{name}")
    public List<User> getAllUsersByName(@PathVariable String name){
        return userService.getAllUsersByName(name);
    }

    @GetMapping("/getById/{id}")
    public User getUserById(@PathVariable String id){
        return userService.getUserById(id);
    }

    @PostMapping("/save")
    public User saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }

    @PostMapping("/update")
    public User updateUser(@RequestBody User user){
        if(getUserById(user.getId()) == null){
            return null;
        }
        return userService.saveUser(user);
    }

    @DeleteMapping("delete/{id}")
    public void deleteUser(@PathVariable String id){
        userService.deleteUser(id);
    }
}
