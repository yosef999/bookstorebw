package com.yosefbw.demo.controller;

import com.yosefbw.demo.entity.Book;
import com.yosefbw.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping("/getAll")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/getAllByTitle/{title}")
    public List<Book> getAllBooksByTitle(@PathVariable String title) {
        return bookService.getAllBooksByTitle(title);
    }

    @GetMapping("/getAllByGenre/{name}")
    public List<Book> getAllBooksByGenre(@PathVariable String name) {
        return bookService.getAllBooksByGenre(name);
    }

    @GetMapping("/{id}")
    public Book getBookById(@PathVariable String id) {
        return bookService.getBookById(id);
    }

    @PostMapping("/save")
    public Book saveBook(@RequestBody Book book){
        return bookService.saveBook(book);
    }

    @PostMapping("/saveAll")
    public @ResponseBody List<Book> saveAllBooks(@RequestBody List<Book> books){
        return bookService.saveAllBooks(books);
    }

    @PostMapping("/update")
    public Book updateBook(@RequestBody Book book){
        if(getBookById(book.getId()) == null){
            return null;
        }
        return bookService.saveBook(book);
    }

    @PostMapping("/updateAll")
    public List<Book> updateAllBooks(@RequestBody List<Book> books){
        for (Book book : books) {
            if(getBookById(book.getId()) == null){
                return null;
            }
        }
        return bookService.saveAllBooks(books);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteBook(@PathVariable String id){
        bookService.deleteBook(id);
    }
}
