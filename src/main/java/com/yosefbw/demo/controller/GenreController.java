package com.yosefbw.demo.controller;

import com.yosefbw.demo.entity.Genre;
import com.yosefbw.demo.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/genre")
public class GenreController {

    @Autowired
    GenreService genreService;

    @GetMapping("/getAll")
    public List<Genre> getAllGenres(){
        return genreService.getAllGenres();
    }

    @GetMapping("/getAllByName/{name}")
    public List<Genre> getAllGenresByName(@PathVariable String name){
        return genreService.getAllGenresByName(name);
    }

    @GetMapping("/getById/{id}")
    public Genre getGenreById(@PathVariable String id){
        return genreService.getGenreById(id);
    }

    @PostMapping("/save")
    public Genre saveGenre(@RequestBody Genre genre){
        return genreService.saveGenre(genre);
    }

    @PostMapping("/saveAll")
    public List<Genre> saveAllGenre(@RequestBody List<Genre> genres){
        return genreService.saveAllGenres(genres);
    }

    @PostMapping("/update")
    public Genre updateGenre(@RequestBody Genre genre){
        if (getGenreById(genre.getId()) == null){
            return null;
        }
        return genreService.saveGenre(genre);
    }

    @PostMapping("/updateAll")
    public List<Genre> updateAllGenre(@RequestBody List<Genre> genres){
        for(Genre genre : genres){
            if (getGenreById(genre.getId()) == null){
                return null;
            }
        }
        return genreService.saveAllGenres(genres);
    }

    @DeleteMapping("/{id}")
    public void deleteGenre(@PathVariable String id){
        genreService.deleteGenre(id);
    }
}
