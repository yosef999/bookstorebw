package com.yosefbw.demo.service;

import com.yosefbw.demo.entity.User;
import com.yosefbw.demo.repository.UserRepository;
import com.yosefbw.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User getUserById(String id) {
        if (userRepository.findById(id).isPresent()){
            return userRepository.findById(id).get();
        } else{
            return null;
        }
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getAllUsersByName(String name) {
        return userRepository.findUsersByName("%" + name + "%");
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }
}
