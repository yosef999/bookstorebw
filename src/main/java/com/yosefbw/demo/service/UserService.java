package com.yosefbw.demo.service;

import com.yosefbw.demo.entity.User;

import java.util.List;

public interface UserService {
    public User saveUser(User user);
    public User getUserById(String id);
    public List<User> getAllUsers();
    public List<User> getAllUsersByName(String name);
    public void deleteUser(String id);
}
