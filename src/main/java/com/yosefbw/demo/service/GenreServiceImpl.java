package com.yosefbw.demo.service;

import com.yosefbw.demo.entity.Genre;
import com.yosefbw.demo.repository.GenreRepository;
import com.yosefbw.demo.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    GenreRepository genreRepository;

    @Override
    public Genre saveGenre(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    public List<Genre> saveAllGenres(List<Genre> genres) {
        return genreRepository.saveAll(genres);
    }

    @Override
    public Genre getGenreById(String id) {
        if(genreRepository.findById(id).isPresent()) {
            return genreRepository.findById(id).get();
        } else {
            return null;
        }
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    @Override
    public List<Genre> getAllGenresByName(String name) {
        return genreRepository.findGenresByName("%" + name.toLowerCase() + "%");
    }

    @Override
    public void deleteGenre(String id) {
        genreRepository.deleteById(id);
    }
}
