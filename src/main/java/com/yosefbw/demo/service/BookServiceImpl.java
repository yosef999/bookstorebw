package com.yosefbw.demo.service;

import com.yosefbw.demo.entity.Book;
import com.yosefbw.demo.repository.BookRepository;
import com.yosefbw.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public List<Book> saveAllBooks(List<Book> books) {
        return bookRepository.saveAll(books);
    }

    @Override
    public Book getBookById(String id) {
        if (bookRepository.findById(id).isPresent()){
            return bookRepository.findById(id).get();
        } else {
            return null;
        }
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> getAllBooksByTitle(String title) {
        return bookRepository.findBooksByTitle("%" + title.toLowerCase() + "%");
    }

    @Override
    public List<Book> getAllBooksByGenre(String name) {
        return bookRepository.findBooksByGenre(name.toLowerCase());
    }

    @Override
    public void deleteBook(String id) {
        bookRepository.deleteById(id);
    }
}
