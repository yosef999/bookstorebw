package com.yosefbw.demo.service;

import com.yosefbw.demo.entity.Book;

import java.util.List;
import java.util.Set;

public interface BookService {
    public Book saveBook(Book book);
    public List<Book> saveAllBooks(List<Book> books);
    public Book getBookById(String id);
    public List<Book> getAllBooks();
    public List<Book> getAllBooksByTitle(String title);
    public List<Book> getAllBooksByGenre(String name);
    public void deleteBook(String id);
}
