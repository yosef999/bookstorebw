package com.yosefbw.demo.service;

import com.yosefbw.demo.entity.Genre;

import java.util.List;

public interface GenreService {
    public Genre saveGenre(Genre genre);
    public List<Genre> saveAllGenres(List<Genre> genres);
    public Genre getGenreById(String id);
    public List<Genre> getAllGenres();
    public List<Genre> getAllGenresByName(String name);
    public void deleteGenre(String id);
}
