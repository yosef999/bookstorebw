package com.yosefbw.demo.repository;

import com.yosefbw.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    @Query(value = "SELECT * FROM User user WHERE user.name LIKE LOWER(:name) OR user.username LIKE LOWER(:name) ", nativeQuery = true)
    List<User> findUsersByName(String name);

}
