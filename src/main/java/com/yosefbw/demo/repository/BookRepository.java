package com.yosefbw.demo.repository;

import com.yosefbw.demo.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, String> {

    @Query(value = "SELECT * FROM Book book WHERE book.title LIKE LOWER(:title) ", nativeQuery = true)
    List<Book> findBooksByTitle(String title);

    @Query(value = "SELECT * FROM Book T1 " +
            "INNER JOIN book_genres T2 ON T1.id = T2.books_id " +
            "INNER JOIN Genre T3 ON T2.genres_id = T3.id WHERE T3.name = LOWER(:name)", nativeQuery = true)
    List<Book> findBooksByGenre(String name);
}
