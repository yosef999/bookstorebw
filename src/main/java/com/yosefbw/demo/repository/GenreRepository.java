package com.yosefbw.demo.repository;

import com.yosefbw.demo.entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, String> {
    @Query(value = "SELECT * FROM Genre genre WHERE genre.name LIKE LOWER(:name) ", nativeQuery = true)
    List<Genre> findGenresByName(String name);


}
